import { equal } from '../node_modules/assert'
import sayHello from '../src/say_hello'

describe('sayHello test',() => {
  it('should return greet with with excitement',() => {
      equal(sayHello('test'), 'Hello test!');
  });
});
